package com.pamproject.pamfidelity.company;

import com.pamproject.pamfidelity.entities.Company;
import com.pamproject.pamfidelity.entities.Customer;
import com.pamproject.pamfidelity.formobject.CompanyAccountForm;

public interface CompanyService {
    Company findCompanyByEmail(String email);
    void saveCompany(Company company);
    void updateCompany(Company company);
    Customer addCustomer(Customer customer);
    Iterable<Company> findCompaniesByCompanyname(String name);
    void updateInfo(Company company, CompanyAccountForm form);

}
