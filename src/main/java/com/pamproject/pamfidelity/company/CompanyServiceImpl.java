package com.pamproject.pamfidelity.company;

import com.pamproject.pamfidelity.entities.Company;
import com.pamproject.pamfidelity.entities.Role;
import com.pamproject.pamfidelity.entities.Customer;
import com.pamproject.pamfidelity.formobject.CompanyAccountForm;
import com.pamproject.pamfidelity.repositories.CompanyRepository;
import com.pamproject.pamfidelity.repositories.RoleRepository;
import com.pamproject.pamfidelity.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    CompanyRepository companyRepo;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RoleRepository roleRepo;


    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Company findCompanyByEmail(String email) {
        return companyRepo.findByEmail(email);
    }

    @Override
    public Customer addCustomer(Customer customer) {
        Customer newCustomer = customerRepository.save(customer);
        return newCustomer;
    }

    @Override
    public Iterable<Company> findCompaniesByCompanyname(String name) {
        return companyRepo.findAllByCompanyname(name);
    }

    @Override
    public void saveCompany(Company company) {
        Role role = roleRepo.findByLabel("COMPANY");
        company.setPassword(bCryptPasswordEncoder.encode(company.getPassword()));
        company.setRole(role);
        company.setActive(1);
        companyRepo.save(company);
    }

    @Override
    public void updateCompany(Company company) {
        companyRepo.save(company);
    }

    @Override public void updateInfo(Company company, CompanyAccountForm form) {
        company.setCompanyname(form.getCompanyName());
        company.setAddress(form.getAddress());
        company.setFax(form.getFax());
        company.setPresentation(form.getPresentation());
        company.setTelephone(form.getPhone());
        company.setWebsite(form.getWebsite());
        companyRepo.save(company);
    }
}
