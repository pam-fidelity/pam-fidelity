package com.pamproject.pamfidelity.customer;

import com.pamproject.pamfidelity.entities.Customer;

public interface CustomerService {

    Customer findCustomerByEmail(String email);
    Customer findCustomerById(long id);
    Iterable<Customer> findAllCustomers();
    Iterable<Customer> searchCustomers(String pattern);


}
