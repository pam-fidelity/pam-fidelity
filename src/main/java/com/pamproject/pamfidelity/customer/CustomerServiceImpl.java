package com.pamproject.pamfidelity.customer;

import com.pamproject.pamfidelity.entities.Customer;
import com.pamproject.pamfidelity.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer findCustomerByEmail(String email) {
        return customerRepository.findByEmailAddress(email);
    }

    @Override
    public Customer findCustomerById(long id) {
        return customerRepository.findById(id);
    }


    @Override
    public Iterable<Customer> findAllCustomers() {
        return customerRepository.findAllByOrderByLastName();
    }

    @Override
    public Iterable<Customer> searchCustomers(String pattern) {
        String sqlPattern = "%"+pattern+"%";
        return customerRepository.findCustomersByFirstNameLikeOrLastNameLike(sqlPattern, sqlPattern);
    }
}
