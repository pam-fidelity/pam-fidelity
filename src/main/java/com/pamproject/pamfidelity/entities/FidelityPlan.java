package com.pamproject.pamfidelity.entities;

import javax.persistence.*;

@Entity
public class FidelityPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private FidelityPlanType fidelityPlanType;

    @Column(nullable = false)
    private Double threshold;

    @Column(nullable = false)
    private Double reward;

    public FidelityPlan() {}

    public FidelityPlan(Long id, FidelityPlanType fidelityPlanType, Double threshold, Double reward) {
        this.id = id;
        this.fidelityPlanType = fidelityPlanType;
        this.threshold = threshold;
        this.reward = reward;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FidelityPlanType getFidelityPlanType() {
        return fidelityPlanType;
    }

    public void setFidelityPlanType(FidelityPlanType fidelityPlanType) {
        this.fidelityPlanType = fidelityPlanType;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public Double getReward() {
        return reward;
    }

    public void setReward(Double reward) {
        this.reward = reward;
    }
}
