package com.pamproject.pamfidelity.entities;


import com.pamproject.pamfidelity.formobject.RegisterForm;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true, nullable = false)
    @Email(message = "Please provide a valid email.")
    @NotEmpty(message = "Please provide an email.")
    private String email;

    @Column(nullable = false)
    @NotEmpty(message = "Please provide a password.")
    private String password;

    @Column(nullable = false)
    @NotEmpty(message = "Please provide a company name")
    private String companyname;

    @Column(nullable = false)
    @NotEmpty(message = "Please provide an address")
    private String address;

    @Column(nullable = false)
    @NotEmpty(message = "Please provide a telephone number")
    private String telephone;

    @Column
    private String pictName;

    @Column
    private String fax;

    @Column
    private String website;

    @Column
    private String openingHours;

    @Column
    private String presentation;

    @Column
    private String miscellaneous;

    @Column
    private String sector; // secteur d'activité

    @OneToOne
    private Role role;

    @Column(nullable = false)
    private int active;

    @OneToOne(cascade=CascadeType.ALL)
    private FidelityPlan fidelityPlan;

    @OneToMany(mappedBy = "company")
    private List<Transaction> transactions;

    protected Company() {}

    public Company(RegisterForm registerForm) {
        this.email = registerForm.getEmail();
        this.password = registerForm.getPassword();
        this.address = registerForm.getAddress();
        this.companyname = registerForm.getCompanyname();
        this.telephone = registerForm.getTelephone();
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public FidelityPlan getFidelityPlan() {
        return fidelityPlan;
    }

    public void setFidelityPlan(FidelityPlan fidelityPlan) {
        this.fidelityPlan = fidelityPlan;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getMiscellaneous() {
        return miscellaneous;
    }

    public void setMiscellaneous(String miscellaneous) {
        this.miscellaneous = miscellaneous;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPictName() {
        return pictName;
    }

    public void setPictName(String pictName) {
        this.pictName = pictName;
    }
}
