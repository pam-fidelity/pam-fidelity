package com.pamproject.pamfidelity.entities;

public enum FidelityPlanType {

    AMOUNT("Montant"),VISITS("Visites");

    FidelityPlanType(String label){
        this.label = label;
    }

    private final String label;

    public String getLabel(){
        return label;
    }
}
