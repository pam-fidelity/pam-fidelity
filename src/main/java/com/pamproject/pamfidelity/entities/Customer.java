package com.pamproject.pamfidelity.entities;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.List;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String firstName;

    @Embedded
    private Address address;

    @Column(unique = true, nullable = false)
    @Email
    private String emailAddress;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String phoneNumber;

    @OneToOne
    private Role role;

    @OneToMany(mappedBy = "customer")
    private List<Transaction> transactions;

    protected Customer() {}

    public Customer(String lastName, String firstName, String emailAddress, String phoneNumber, int number, String street, int postalCode, String city, String country) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.address = new Address(number, street, postalCode, city, country);
        this.password = "motdepasse987";
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, lastName=%s, firstName=%s, emailAddress=%s, phoneNumber=%s, address=%s]",
                id, lastName, firstName, emailAddress, phoneNumber, address);
    }

    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
