package com.pamproject.pamfidelity.formobject;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class RegisterForm {

    @NotEmpty(message = "Vous devez entrer une adresse email.")
    @Email(message = "L'adresse email n'est pas valide.")
    private String email;

    @NotEmpty(message = "Vous devez entrer un mot de passe.")
    private String password;

    @NotEmpty(message = "Vous devez recopier votre mot de passe.")
    private String confirm;

    @NotEmpty(message = "Vous devez entrer le nom de votre entreprise.")
    private String companyname;

    @NotEmpty(message = "Vous devez entrer l'adresse de votre entreprise.")
    private String address;

    @NotEmpty(message = "Vous devez entrer le numéro de téléphone de votre entreprise")
    private String telephone;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

}
