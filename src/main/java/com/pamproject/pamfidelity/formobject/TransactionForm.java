package com.pamproject.pamfidelity.formobject;

import javax.validation.constraints.NotNull;

public class TransactionForm {

    @NotNull(message = "Vous devez entrer un numéro.")
    private Double amount;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
