package com.pamproject.pamfidelity.formobject;

import com.pamproject.pamfidelity.entities.Company;
import org.hibernate.validator.constraints.NotEmpty;

public class CompanyAccountForm {
    @NotEmpty
    private String companyName;
    @NotEmpty
    private String phone;
    private String fax;
    private String address;
    private String website;
    private String presentation;

    public CompanyAccountForm() {}

    public CompanyAccountForm(Company company) {
        companyName = company.getCompanyname();
        phone = company.getTelephone();
        fax = company.getFax();
        address = company.getAddress();
        website = company.getWebsite();
        presentation = company.getPresentation();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }
}
