package com.pamproject.pamfidelity.formobject;

import com.pamproject.pamfidelity.entities.Address;
import com.pamproject.pamfidelity.validators.ValidationRegex;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CustomerRegisterForm {

    @NotEmpty(message = "Vous devez entrer un nom.")
    @Pattern(regexp = ValidationRegex.NAME_REGEX, message = "Nom non valide")
    private String lastName;

    @NotEmpty(message = "Vous devez entrer un prénom.")
    @Pattern(regexp = ValidationRegex.NAME_REGEX, message = "Prénom non valide")
    private String firstName;

    @NotEmpty(message = "Vous devez entrer une adresse email.")
    @Email(message = "Email non valide")
    private String emailAddress;

    @NotEmpty(message = "Vous devez entrer un numéro de téléphone.")
    @Pattern(regexp = ValidationRegex.PHONE_REGEX, message = "Téléphone non valide")
    private String phoneNumber;

    @NotNull(message = "Vous devez entrer un numéro.")
    private int number;

    @NotEmpty(message = "Vous devez entrer une adresse.")
    private String street;

    @NotNull(message = "Vous devez entrer un code postal.")
    private int postalCode;

    @NotEmpty(message = "Vous devez entrer une ville.")
    private String city;

    @NotEmpty(message = "Vous devez entrer un pays.")
    private String country;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
