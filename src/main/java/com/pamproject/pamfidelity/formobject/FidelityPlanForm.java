package com.pamproject.pamfidelity.formobject;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class FidelityPlanForm {

    @NotEmpty(message = "Vous devez entrer un type de plan.")
    private String fidelityPlanType;

    @NotNull(message = "Vous devez entrer un numéro.")
    private Double threshold;

    @NotNull(message = "Vous devez entrer un numéro.")
    private Double reward;

    public String getFidelityPlanType() {
        return fidelityPlanType;
    }

    public void setFidelityPlanType(String fidelityPlanType) {
        this.fidelityPlanType = fidelityPlanType;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public Double getReward() {
        return reward;
    }

    public void setReward(Double reward) {
        this.reward = reward;
    }
}
