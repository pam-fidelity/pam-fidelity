package com.pamproject.pamfidelity.transaction;

import com.pamproject.pamfidelity.entities.Company;
import com.pamproject.pamfidelity.entities.Customer;
import com.pamproject.pamfidelity.entities.Transaction;
import com.pamproject.pamfidelity.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Transaction findTransactionById(long id) {
        return transactionRepository.findById(id);
    }

    @Override
    public Iterable<Transaction> findAllByCompanyIdAndCustomerId(long companyId, long customerId) {
        return transactionRepository.findAllByCompanyIdAndCustomerId(companyId,customerId);
    }

    @Override
    public Transaction findLastTransactionByCompanyIdAndCustomerId(long companyId, long customerId) {
        ArrayList<Transaction> transactions = (ArrayList)findAllByCompanyIdAndCustomerId(companyId,customerId);
        if(transactions.isEmpty()){
            return null;
        }else{
            Transaction transaction = transactions.get(0);
            LocalDateTime date = transaction.getDateTime();
            for (Transaction t: transactions) {
                if(t.getDateTime().isAfter(date))
                    transaction = t;
            }
            return transaction;
        }
    }

    @Override
    public Transaction createTransactionByCompanyAndCustomer(Company company, Customer customer, Double amount) {
        Transaction transaction = new Transaction();
        transaction.setDateTime(LocalDateTime.now());
        transaction.setCompany(company);
        transaction.setAmount(amount);
        transaction.setCustomer(customer);
        Transaction lastTransaction = findLastTransactionByCompanyIdAndCustomerId(company.getId(),customer.getId());
        transaction.setBalance(lastTransaction == null ? amount : lastTransaction.getBalance()+amount);
        transactionRepository.save(transaction);
        return transaction;
    }

}
