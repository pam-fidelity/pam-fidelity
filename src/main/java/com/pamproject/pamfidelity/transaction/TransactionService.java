package com.pamproject.pamfidelity.transaction;

import com.pamproject.pamfidelity.entities.Company;
import com.pamproject.pamfidelity.entities.Customer;
import com.pamproject.pamfidelity.entities.Transaction;

public interface TransactionService {
    Transaction findTransactionById(long id);
    Iterable<Transaction> findAllByCompanyIdAndCustomerId(long companyId, long customerId);
    Transaction findLastTransactionByCompanyIdAndCustomerId(long companyId, long customerId);
    Transaction createTransactionByCompanyAndCustomer(Company company, Customer customer, Double amount);
}
