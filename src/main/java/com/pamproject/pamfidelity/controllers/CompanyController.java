package com.pamproject.pamfidelity.controllers;

import com.pamproject.pamfidelity.company.CompanyService;
import com.pamproject.pamfidelity.customer.CustomerService;
import com.pamproject.pamfidelity.entities.*;
import com.pamproject.pamfidelity.formobject.CompanyAccountForm;
import com.pamproject.pamfidelity.formobject.CustomerRegisterForm;
import com.pamproject.pamfidelity.formobject.FidelityPlanForm;
import com.pamproject.pamfidelity.formobject.TransactionForm;
import com.pamproject.pamfidelity.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TransactionService transactionService;

    @ModelAttribute("username")
    public String getUsername(Authentication authentication) {
        return authentication.getName();
    }

    @GetMapping("/addCustomer")
    public String addCustomer(Model model, Authentication authentication) {
        model.addAttribute("customerRegisterForm", new CustomerRegisterForm());
        return "customerRegister";
    }

    @PostMapping("/addCustomer")
    public String addCustomer(@Valid CustomerRegisterForm form, BindingResult result) {

        //We check if the email address is already used
        Customer customer = customerService.findCustomerByEmail(form.getEmailAddress());
        if(customer != null) {
            result.rejectValue("email", "error.email", "Cette adresse email est déjà utilisée.");
        }
        //If there are any errors
        if(result.hasErrors()) {
            return "customerRegister";
        } else {
            //Else we register this account and redirect to the Log In page.
            companyService.addCustomer(
                    new Customer(form.getLastName(),form.getFirstName(),form.getEmailAddress(),
                    form.getPhoneNumber(),form.getNumber(),form.getStreet(),
                    form.getPostalCode(), form.getCity(), form.getCountry()));
            return "redirect:/company/customers";
        }
    }

    @ModelAttribute("allTypes")
    public FidelityPlanType[] registerContact() {
        return FidelityPlanType.values();
    }

    //Page to create a new fidelity plan or modify the existing one
    @GetMapping("/manageFidelityPlan")
    public String manageFidelityPlan(Model model, Authentication authentication){
        Company company = companyService.findCompanyByEmail(authentication.getName());
        FidelityPlan fidelityPlan = company.getFidelityPlan();
        FidelityPlanForm fidelityPlanForm = new FidelityPlanForm();
        if(fidelityPlan != null){
            fidelityPlanForm.setFidelityPlanType(fidelityPlan.getFidelityPlanType().name());
            fidelityPlanForm.setThreshold(fidelityPlan.getThreshold());
            fidelityPlanForm.setReward(fidelityPlan.getReward());
        }
        model.addAttribute("fidelityPlanForm", fidelityPlanForm);
        return "manageFidelityPlan";
    }

    @PostMapping("/manageFidelityPlan")
    public String manageFidelityPlan(@Valid FidelityPlanForm form, BindingResult result, Authentication authentication) {
        //If there are any errors
        if(result.hasErrors()) {
            return "manageFidelityPlan";
        } else {
            //Else we register this fidelity plan
            Company company = companyService.findCompanyByEmail(authentication.getName());
            FidelityPlan fidelityPlan = company.getFidelityPlan();
            if(fidelityPlan == null){
                fidelityPlan = new FidelityPlan();
            }
            fidelityPlan.setFidelityPlanType(FidelityPlanType.valueOf(form.getFidelityPlanType()));
            fidelityPlan.setThreshold(form.getThreshold());
            fidelityPlan.setReward(form.getReward());
            company.setFidelityPlan(fidelityPlan);
            companyService.updateCompany(company);
            return "redirect:/company/customers";
        }
    }

    @GetMapping("/customers")
    public String listCustomers(@RequestParam("pattern") Optional<String> pattern, Model model, Authentication authentication) {
        Company company = companyService.findCompanyByEmail(authentication.getName());
        Iterable<Customer> customers;
        if(pattern.isPresent()) {
            customers = customerService.searchCustomers(pattern.get());
        } else {
            customers = customerService.findAllCustomers();
        }
        model.addAttribute("customers", customers);
        model.addAttribute("hasFidelityPlan", company.getFidelityPlan() != null );
        return "customersList";
    }

    @GetMapping("/profile")
    public String companyProfile(Model model, Authentication authentication) {
        Company company = companyService.findCompanyByEmail(authentication.getName());
        model.addAttribute("company", company);
        return "companyProfile";
    }

    @GetMapping("/account")
    public String companyAccount(Model model, Authentication authentication) {
        Company company = companyService.findCompanyByEmail(authentication.getName());
        CompanyAccountForm form = new CompanyAccountForm(company);
        model.addAttribute("companyForm", form);
        return "companyAccount";
    }

    @PostMapping("/account")
    public String companyAccount(@Valid CompanyAccountForm form, Authentication authentication) {
        Company company = companyService.findCompanyByEmail(authentication.getName());
        companyService.updateInfo(company, form);
        return "redirect:/company/account";
    }

    @GetMapping("/customerDetails/{idCustomer}")
    public String customerDetails(@PathVariable("idCustomer") long idCustomer, Model model, Authentication authentication){
        Customer customer = customerService.findCustomerById(idCustomer);
        Company company = companyService.findCompanyByEmail(authentication.getName());
        List<Transaction> transactions = (List<Transaction>) transactionService.findAllByCompanyIdAndCustomerId(company.getId(),customer.getId());
        FidelityPlan fidelityPlan = company.getFidelityPlan();
        if(customer == null){
            return "redirect:/company/customers";
        }
        model.addAttribute("customerName",customer.getFirstName()+" "+customer.getLastName().toUpperCase());
        model.addAttribute("customer", customer);
        model.addAttribute("company", company);
        model.addAttribute("transactions", transactions);
        model.addAttribute("hasFidelityPlan", fidelityPlan != null );
        return "customerDetails";
    }

    @GetMapping("/credit/{idCustomer}")
    public String creditCustomer(@PathVariable("idCustomer") long idCustomer, Model model, Authentication authentication){
        Customer customer = customerService.findCustomerById(idCustomer);
        Company company = companyService.findCompanyByEmail(authentication.getName());
        FidelityPlan fidelityPlan = company.getFidelityPlan();
        if(customer == null || fidelityPlan == null){
            return "redirect:/company/customers";
        }
        model.addAttribute("customerName",customer.getFirstName()+" "+customer.getLastName().toUpperCase());
        model.addAttribute("idCustomer", idCustomer);
        model.addAttribute("transactionForm", new TransactionForm());
        return "credit";
    }

    @PostMapping("/credit/{idCustomer}")
    public String creditCustomer(@PathVariable("idCustomer") long idCustomer, @Valid TransactionForm form, Authentication authentication){
        Customer customer = customerService.findCustomerById(idCustomer);
        Company company = companyService.findCompanyByEmail(authentication.getName());
        FidelityPlan fidelityPlan = company.getFidelityPlan();
        if(customer == null || fidelityPlan == null){
            return "ERROR";
        }
        transactionService.createTransactionByCompanyAndCustomer(company,customer,form.getAmount());
        return "redirect:/company/customers";
    }

}
