package com.pamproject.pamfidelity.repositories;

import com.pamproject.pamfidelity.entities.Transaction;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository  extends CrudRepository<Transaction, Long> {
    Transaction findById(long id);
    Iterable<Transaction> findAllByCompanyIdAndCustomerId(long companyId, long customerId);
}
