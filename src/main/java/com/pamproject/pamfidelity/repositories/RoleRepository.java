package com.pamproject.pamfidelity.repositories;

import com.pamproject.pamfidelity.entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByLabel(String label);
}
