package com.pamproject.pamfidelity.repositories;

import com.pamproject.pamfidelity.entities.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
    Customer findByEmailAddress(String email);
    Customer findById(long id);
    Iterable<Customer> findAllByOrderByLastName();
    Iterable<Customer> findCustomersByFirstNameLikeOrLastNameLike(String firstNamePattern, String lastNamePatter);
}
