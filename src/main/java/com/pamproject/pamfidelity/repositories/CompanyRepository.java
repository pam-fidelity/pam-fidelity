package com.pamproject.pamfidelity.repositories;

import com.pamproject.pamfidelity.entities.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
    Company findByEmail(String email);

    Iterable<Company> findAllByCompanyname(String name);
}
