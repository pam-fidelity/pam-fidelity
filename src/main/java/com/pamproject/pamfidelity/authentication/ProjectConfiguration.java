package com.pamproject.pamfidelity.authentication;

import com.pamproject.pamfidelity.entities.Role;
import com.pamproject.pamfidelity.repositories.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Configuration
@Component
public class ProjectConfiguration {

    @Autowired
    public RoleRepository roleRepository;

    private static final Logger log =
            LoggerFactory.getLogger(ProjectConfiguration.class);

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        if(roleRepository.count()<2){
            Role company = new Role();
            company.setLabel("COMPANY");
            Role customer = new Role();
            customer.setLabel("CUSTOMER");
            roleRepository.save(company);
            roleRepository.save(customer);
            log.info("Roles créés");
        }else{
            log.info("Roles déjà existants");
        }
    }
}