package com.pamproject.pamfidelity.authentication;

import com.pamproject.pamfidelity.company.CompanyService;
import com.pamproject.pamfidelity.entities.Company;
import com.pamproject.pamfidelity.formobject.RegisterForm;
import com.pamproject.pamfidelity.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class AuthenticationController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CompanyService companyService;

    @GetMapping(path = "/")
    public String home() {
        return "home";
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid RegisterForm registerForm, BindingResult result) {

        //We check if the email address is already used
        Company company = companyService.findCompanyByEmail(registerForm.getEmail());
        if(company != null) {
            result.rejectValue("email", "error.email", "Cette adresse email est déjà utilisée.");
        }

        //We check if the 2 passwords are the same
        if(!registerForm.getPassword().equals(registerForm.getConfirm())) {
            result.rejectValue("confirm", "error.confirm", "Les mots de passe ne correspondent pas.");
        }

        //We check if the name is already taken
        if( companyService.findCompaniesByCompanyname(registerForm.getCompanyname()).iterator().hasNext() )
        {
            result.rejectValue("companyname", "error.confirm", "Une entreprise porte déjà ce nom.");
        }
        
        //If there are any errors
        if(result.hasErrors()) {
            return "register";
        } else {
            //Else we register this account and redirect to the Log In page.
            Company c = new Company(registerForm);
            c.setRole(roleRepository.findByLabel("COMPANY"));
            companyService.saveCompany(c);
            return "redirect:login";
        }
    }

}
