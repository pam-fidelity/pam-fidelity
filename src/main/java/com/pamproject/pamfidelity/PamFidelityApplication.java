package com.pamproject.pamfidelity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@EntityScan(
		basePackageClasses = {PamFidelityApplication.class, Jsr310JpaConverters.class}
)

@SpringBootApplication
public class PamFidelityApplication {

	public static void main(String[] args) {
		SpringApplication.run(PamFidelityApplication.class, args);
	}

}
