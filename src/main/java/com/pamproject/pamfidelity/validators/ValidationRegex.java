package com.pamproject.pamfidelity.validators;

import com.pamproject.pamfidelity.entities.Customer;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ValidationRegex {

    public static final String NAME_REGEX = "^[\\p{L} .'-]+$";

    public static final String PHONE_REGEX = "^\\+?[0-9. ()-]{10,25}$";

}
